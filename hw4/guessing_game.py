from random import randint


def guessing_game(low, high):
    mystery_num = randint(low, high)
    guessed_num = high + 2
    while guessed_num != mystery_num:
        guessed_num = int(input('Guess a number from ' + str(low) + ' to ' + str(high) +': '))
        if guessed_num < mystery_num:
            print('The mystery number is smaller')
        elif guessed_num > mystery_num:
            print('The mystery number is larger')
    print('You won!')
