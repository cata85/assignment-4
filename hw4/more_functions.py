def odd_even_merge(list1, list2):
    if len(list1) == 0 or len(list2) == 0:
        return []
    return list(filter((lambda x: x % 2 == 1), list1) + filter((lambda x: x % 2 == 0), list2))


def make_dict(a_list):
    diction = {}
    for i in range(len(a_list)):
        diction['key' + str(i)] = a_list[i]
    return diction


def transpose_matrix(matrix):
    return zip(*matrix)


def merge_dicts(dict1, dict2):
    return dict(dict1.items() + dict2.items())


def squares(start, qty):
    return list(map(lambda x: x**2, range(start, start + qty)))
