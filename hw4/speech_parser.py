def parse_text(
        filename,
        write_to_file=True,
        output_filename='out.txt',
        ignore_file='ignore.txt',
        top=10):
    # tries to open the user's text file, throws exception if file not found
    try:
        with open(filename, 'r') as txt:
            text = txt.readline().lower()
            with open(ignore_file, 'r') as ign:
                ignore = list(ign.readline().split())
    except ValueError:
        print('Could not open text file!')
    # formats the speech text file for 'easier' management for the dictionary
    text = replace_text(text, ignore)
    diction = dictionary_counter(text)
    # clears file if held previous text
    if write_to_file: clear_file(output_filename)
    top_10(diction, top, output_filename, write_to_file)


# eliminates the ignored words from the speech text
def replace_text(txt, ignore):
    for x in ignore:
        if len(x) > 1:
            txt = txt.replace(' ' + x + ' ', ' ')
        else:
            txt = txt.replace(x, '')
    return txt


# returns a dictionary of unique words and how many times they occur
def dictionary_counter(txt):
    diction = {}
    for word in txt.split():
        if diction.get(word) is None:
            diction[word] = 1
        else:
            diction[word] += 1
    return diction


# prints the top 10 or however many words
def top_10(diction, top, output_filename, write_to_file):
    for x in range(top):
        dict_to_print = reduce(lambda x, y: x if diction.get(x) > diction.get(y) else y, diction)
        if write_to_file:
            with open(output_filename, 'a') as f:
                f.write(str(x + 1) + '. ' + str(dict_to_print) + ': ' + str(diction.get(dict_to_print)) + '\n')
        else:
            print(str(x + 1) + '. ' + str(dict_to_print) + ': ' + str(diction.get(dict_to_print)))
        diction[dict_to_print] = 0


def clear_file(output_filename):
    with open(output_filename, 'w') as clear:
        clear.write('')