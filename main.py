from hw4 import more_functions
from hw4 import guessing_game
from hw4 import speech_parser

print(more_functions.odd_even_merge([1, 2, 3, 4, 5, 6], [7, 8, 9, 10, 11, 12, 13]))
print(more_functions.make_dict([1, 5, 2, 7]))
print(more_functions.transpose_matrix([[1, 2], [5, 6]]))
print(more_functions.merge_dicts({'hello': 1, 'goodbye': 2}, {'yes': 3, 'no': 4}))
print(more_functions.squares(1, 3))

guessing_game.guessing_game(0, 20)

speech_parser.parse_text('JustDoIt.txt')
